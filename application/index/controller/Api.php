<?php
namespace app\index\controller;
use think\Controller;
use think\Hook;
use	app\index\model\User;
use app\index\model\Customer as Customers;
use app\index\model\Warehouse as Warehouses;
use app\index\model\Goods as Goodss;
use app\index\model\Serialinfo;
use app\index\model\Saleclass;
use app\index\model\Saleinfo;
use app\index\controller\Sale;
use app\index\model\Room;
use app\index\model\Roominfo;
use app\index\model\Account;
use app\index\model\Accountinfo;
use app\index\model\Salebill;
use app\index\model\User as Users;
class Api extends Controller{
     //获取版本号
     public function resetPassword(){
        $input=input('post.');
        $update_info=Users::update(syn_sql($input,'user'));
        if( $update_info){
            $resule=[
                'code'=>1,
                'state'=>'success','info'=>'密码更新成功'];
        }else{
            $resule=[
                'code'=>0,
                'state'=>'error','info'=>'密码更新失败'];
        }
     
        return json($resule);
    }
    //获取版本号
    public function getVersion(){
        $resule=[
            'code'=>1,
            'data'=>[
                'version'=>'1.0.1',
                'base_url'=>'/apks/app.apk',
                'exp_url'=>'/apks/app.wgt',
            ],
            'state'=>'error','info'=>'获取版本成功'];
        return json($resule);
    }
     //获取销货单详情
     public function getStatistics(){
        $input=input('get.');
        //数据完整性判断
        if(isset_full($input,'id')){
            $class=Saleclass::where(['id'=>$input['id']])->find();
            $info=Saleinfo::with('roominfo,goodsinfo,warehouseinfo')->where(['pid'=>$input['id']])->select()->toarray();
            foreach ($info as $info_key=>$info_vo) {
                //改造串码数据
                $info[$info_key]['roominfo']['serialinfo']=implode(",",arraychange(searchdata($info_vo['roominfo']['serialinfo'],['type|nod'=>['eq',0]]),'code'));
            }
            $resule=[  'code'=>1,'state'=>'success','data'=>['info'=>$info,'cla'=>$class]];
        }else{
            $resule=[  'code'=>0,'state'=>'error','info'=>'传入参数不完整!'];
        }
        return json($resule);
    }
    //获取销货单详情
    public function info(){
        $input=input('get.');
        //数据完整性判断
        if(isset_full($input,'id')){
            $class=Saleclass::where(['id'=>$input['id']])->find();
            $info=Saleinfo::with('roominfo,goodsinfo,warehouseinfo')->where(['pid'=>$input['id']])->select()->toarray();
            foreach ($info as $info_key=>$info_vo) {
                //改造串码数据
                $info[$info_key]['roominfo']['serialinfo']=implode(",",arraychange(searchdata($info_vo['roominfo']['serialinfo'],['type|nod'=>['eq',0]]),'code'));
            }
            $resule=[  'code'=>1,'state'=>'success','data'=>['info'=>$info,'cla'=>$class]];
        }else{
            $resule=[  'code'=>0,'state'=>'error','info'=>'传入参数不完整!'];
        }
        return json($resule);
    }
    //获取销货单列表
    public function form_list(){
        $input=input('post.');
        //数据完整性判断
        if(isset_full($input,'page') && isset_full($input,'limit')){
            $sql=get_sql($input,[
                'name'=>'continue',
                'number'=>'full_like',
                'customer'=>'full_division_in',
                'start_time'=>'stime',
                'end_time'=>'etime',
                'type'=>'full_dec_1',
                'warehouse'=>'continue',
                'user'=>'full_division_in',
                'account'=>'full_division_in',
                'data'=>'full_like',
            ],'saleclass');//构造SQL
            //处理名称搜索
            if(isset_full($input,'name')){
                $goods=get_db_field('goods',['name|py'=>['like','%'.$input['name'].'%']],'id');//取出商品表信息
                if(empty($goods)){
                    sql_assign($sql,'id',[]);//多表查询赋值处理
                }else{
                    $info=get_db_field('saleinfo',['goods'=>['in',$goods]],'pid');//取出详情表数据
                    sql_assign($sql,'id',$info);//多表查询赋值处理
                }
            }
            //处理仓库搜索
            if(isset_full($input,'warehouse')){
                $info=get_db_field('saleinfo',['warehouse'=>['in',explode(",",$input['warehouse'])]],'pid');//取出详情表数据
                sql_assign($sql,'id',$info,'intersect');//多表查询赋值处理
            }
            if(isset($input['merchant'])&&$input['merchant']>0){
                $sql['merchant']=['=',$input['merchant']];//补全授权商户数据
            }
            $sql=auth('saleclass',$sql);//数据鉴权
            $count = Saleclass::where ($sql)->count();//获取总条数
            if($count>0){
                $arr = Saleclass::with('merchantinfo,customerinfo,userinfo,accountinfo')->where($sql)->page($input['page'],$input['limit'])->order('id desc')->select();//查询分页数据
                $resule=[
                    'code'=>1,
                    'msg'=>'获取成功',
                    'count'=>$count,
                    'data'=>$arr
                ];//返回数据s
            }else{
                $resule=[
                    'code'=>0,
                    'msg'=>'没有找到对应的数据',
                    'count'=>$count,
                ];//返回数据s
            }
            
        }else{
            $resule=['state'=>'error','info'=>'传入参数不完整!'];
        }
        return json($resule);
    }
    //获取商品信息
    public function get_goods(){
        $input=input('post.');
        if(isset_full($input,'code')){
            $goodsData=Goodss::with('classinfo,unitinfo,brandinfo,warehouseinfo,attrinfo')->where(['code'=>$input['code']])->find();
            $resule=['code'=>'1','state'=>'success','data'=>$goodsData];
        }else{
            $resule=['code'=>'0','state'=>'error','info'=>'传入参数不完整!'];
        }
        return json($resule);
    }
    //新增|更新销货单
    public function set(){
        $input=input('post.');
        if(isset($input['id'])){
            //验证销货单详情
            if(isset_full($input,'tab')){
                foreach ($input['tab'] as $tab_key=>$tab_vo) {
                    $tab_vali = $this->validate($tab_vo,'Saleinfo');//详情验证
                    if($tab_vali!==true){
                        return json(['code'=>'0','state'=>'error','info'=>'[ 数据表格 ]第'.($tab_key+1).'行'.$tab_vali]);
                        exit;
                    }
                }
            }else{
                return json(['code'=>'0','state'=>'error','info'=>'数据表格不可为空!']);
                exit;
            }
            //验证操作类型
            if(empty($input['id'])){
                $vali = $this->validate($input,'Saleclass');
                if($vali===true){
                    $create_info=Saleclass::create(syn_sql($input,'saleclass'));
                    Hook::listen('create_sale',$create_info);//销货单新增行为
                    //push_log('新增销货单[ '.$create_info['number'].' ]');//日志
                    $resule=['code'=>'1','state'=>'success'];
                }else{
                    $resule=['code'=>'0','state'=>'error','info'=>$vali];
                }
            }else{
                //更新
                $vali = $this->validate($input,'Saleclass.update');
                if($vali===true){
                    $update_info=Saleclass::update(syn_sql($input,'saleclass'));
                    Hook::listen('update_sale',$update_info);//销货单更新行为
                    //push_log('更新销货单[ '.$update_info['number'].' ]');//日志
                    Saleinfo::where(['pid'=>$update_info['id']])->delete();
                    $resule=['code'=>'1','state'=>'success'];
                }else{
                    $resule=['code'=>'0','state'=>'error','info'=>$vali];
                }
            }
            //添加销货单详情
            if($resule['state']=='success'){
                $info_pid=empty($input['id'])?$create_info['id']:$update_info['id'];
                foreach ($input['tab'] as $info_vo) {
                    $info_vo['pid']=$info_pid;
                    (isset_full($info_vo,'serial')&& $info_vo['serial']=='&amp;nbsp;')&&($info_vo['serial']='');//兼容串码
                    Saleinfo::create(syn_sql($info_vo,'saleinfo'));
                }
            }
        }else{
            $resule=['code'=>'0','state'=>'error','info'=>'传入参数不完整!'];
        }
        //兼容自动审核[新增操作]
        if($resule['state']=='success'&&empty($input['id'])){
            empty(get_sys(['auto_auditing']))||($this->auditing([$create_info['id']],true));
        }
        return json($resule);
    }
     //审核
     public function auditing($arr=[],$auto=false){
        (empty($arr))&&($arr=input('post.arr'));//兼容多态审核
        if(empty($arr)){
            $resule=['state'=>'error','info'=>'传入参数不完整!'];
        }else{
            $class_data=[];//初始化CLASS数据
            $info_data=[];//初始化INFO数据
            //数据检验
            foreach ($arr as $arr_vo) {
                $class=Saleclass::where(['id'=>$arr_vo])->find();
                $info=Saleinfo::where(['pid'=>$arr_vo])->select();
                //判断操作类型
                if(empty($class['type']['nod'])){
                    //审核操作
                    foreach ($info as $info_key=>$info_vo) {
                        if(!empty($info_vo['serial'])){
                            $serial_sql=['code'=>['in',explode(',',$info_vo['serial'])],'type'=>['neq',0]];
                            $serial=Serial::where($serial_sql)->find();//查找串码状态为非未销售
                            if(!empty($serial)){
                                //$auto&&(push_log('自动审核销货单[ '.$class['number'].' ]失败,原因:第'.($info_key+1).'行串码状态不正确!'));//日志
                                return json(['state'=>'error','info'=>'审核-销货单[ '.$class['number'].' ]失败,原因:第'.($info_key+1).'行串码状态不正确!']);
                                exit;
                            }
                        }
                    }
                }else{
                    //反审核操作
                    foreach ($info as $info_key=>$info_vo) {
                        if(!empty($info_vo['serial'])){
                            $serial_sql=['code'=>['in',explode(',',$info_vo['serial'])],'type'=>['neq',1]];
                            $serial=Serial::where($serial_sql)->find();//查找串码状态为非已销售
                            if(!empty($serial)){
                                return json(['state'=>'error','info'=>'反审核-销货单[ '.$class['number'].' ]第'.($info_key+1).'行串码状态不正确!']);
                                exit;
                            }
                        }
                    }
                }
                $class_data[$arr_vo]=$class;//转存CLASS数据
                $info_data[$arr_vo]=$info;//转存INFO数据
            }
            //实际操作
            foreach ($arr as $arr_vo) {
                $class=$class_data[$arr_vo];//读取CLASS数据
                $info=$info_data[$arr_vo];//读取INFO数据
                //判断操作类型
                if(empty($class['type']['nod'])){
                    //审核操作
                    foreach ($info as $info_vo) {
                        //设置仓储信息
                        Room::where(['id'=>$info_vo['room']])->setDec('nums',$info_vo['nums']);//更新仓储数据[-]
                        //新增仓储详情
                        $roominfo_sql['pid']=$info_vo['room'];
                        $roominfo_sql['type']=2;
                        $roominfo_sql['class']=$arr_vo;
                        $roominfo_sql['info']=$info_vo['id'];
                        $roominfo_sql['nums']=$info_vo['nums'];
                        Roominfo::create($roominfo_sql);
                        //操作串码信息
                        if (!empty($info_vo['serial'])){
                            $serial_arr=explode(',',$info_vo['serial']);//分割串码信息
                            foreach ($serial_arr as $serial_arr_vo) {
                                //设置串码信息
                                $serial=Serial::where(['code'=>$serial_arr_vo])->find();//获取串码信息
                                Serial::update(['id'=>$serial['id'],'type'=>1]);
                                //新增串码详情
                                Serialinfo::create (['pid'=>$serial['id'],'type'=>2,'class'=>$arr_vo]);
                            }
                        }
                    }
                    //获取核销状态
                    if($class['money']==$class['actual']){
                        $billtype=2;//已核销
                    }elseif($class['money']==0){
                        $billtype=0;//未核销
                    }else {
                        $billtype=1;//部分核销
                    }
                    //操作核销信息
                    if (!empty($class['money'])){
                        $input=input('post.');
                        //新增对账单
                        $bill=Salebill::create(['pid'=>$arr_vo,'account'=>$class['account'],'money'=>$class['money'],'data'=>'系统自动生成','user'=>$input['user'],'time'=>time()]);
                        Account::where (['id'=>$class['account']])->setInc('balance',$class['money']);//操作资金账户[+]
                        Accountinfo::create (['pid'=>$class['account'],'set'=>1,'money'=>$class['money'],'type'=>2,'time'=>time(),'user'=>$input['user'],'class'=>$arr_vo,'bill'=>$bill['id']]);//新增资金详情
                    }
                    Saleclass::update(['id'=>$arr_vo,'type'=>1,'auditinguser'=>Session('is_user_id'),'auditingtime'=>time(),'billtype'=>$billtype]);//更新CLASS数据
                    set_summary('sale',$arr_vo,true);//更新统计表
                    //push_log(($auto?'自动':'').'审核销货单[ '.$class['number'].' ]');
                }else{
                    //反审核操作
                    foreach ($info as $info_vo){
                        Room::where (['id'=>$info_vo['room']])->setInc('nums',$info_vo['nums']);//更新仓储数据[+]
                        if(!empty($info_vo['serial'])){
                            $serial=Serial::where(['code'=>['in',explode(',',$info_vo['serial'])]])->select();//获取串码数据
                            foreach ($serial as $serial_vo) {
                                //设置串码数据
                                Serial::update(['id'=>$serial_vo['id'],'type'=>0]);
                                Serialinfo::where(['pid'=>$serial_vo['id'],'type'=>2,'class'=>$arr_vo])->delete();//删除串码详情
                            }
                        }
                    }
                    Roominfo::where(['type'=>2,'class'=>$arr_vo])->delete();//删除仓储详情
                    //操作核销信息
                    if (!empty($class['money'])){
                        $bill=Salebill::where(['pid'=>$arr_vo])->select();
                        foreach ($bill as $bill_vo){
                            Account::where(['id'=>$bill_vo['account']])->setDec('balance',$bill_vo['money']);//操作资金账户[-]
                        }
                        Accountinfo::destroy (['type'=>2,'class'=>$arr_vo]);//删除资金详情
                        Salebill::destroy(['pid'=>$arr_vo]);//删除对账单信息
                    }
                    Saleclass::update(['id'=>$arr_vo,'type'=>0,'money'=>0,'auditinguser'=>0,'auditingtime'=>0,'billtype'=>-1]);//更新CLASS数据
                    set_summary('sale',$arr_vo,false);//更新统计表
                   // push_log ('反审核销货单[ '.$class['number'].' ]');
                }
            }
            $resule=['state'=>'success'];
        }
        return $auto?true:json($resule);
    }
   //验证用户账号密码
   public function check_user(){
        $input=input('post.');
        if(isset_full($input,'user')&&isset_full($input,'pwd')){
            $sql=get_sql($input,['pwd'=>'md5'],'user');//构造SQL
            $user = User::get($sql);
            if($user){
                $token=user_token();
                $user->token=$token;
                $user->save();
                //设置登录
                cookie('Nod_User_Id',$user['id']);
                cookie('Nod_User_Token',$token);
                Session('is_user_id',$user['id']);
                Session('is_merchant_id',$user['merchant']);
                push_log('登录系统成功');//日志
                Hook::listen('login_success',$user);//登录成功行为
                //通用操作-开始
                del_time_file ('skin/upload/xlsx/');//删除超时文件
                del_time_file ('skin/images/code/');//删除超时文件
                $resultUser['user_id']=$user['id'];
                $resultUser['user_name']=$user['name'];
                $resultUser['user_acc']=$user['user'];
                $resultUser['user_merchant']=$user['merchant'];
                $resultUser['merch_type']=$user['merchantinfo']['type'];
                $resultUser['token']=$token;
                //通用操作-结束
                return json(['code'=>'1','state'=>'success','data'=>$resultUser]);
            }else{
                $hookinfo=['user'=>$input['user'],'pwd'=>$input['pwd']];//钩子信息
                Hook::listen('login_error',$hookinfo);//登录失败行为
                return json(['code'=>'1','state'=>'error','info'=>'账号或密码错误,请核实!']);
            }
        }else{
            return json(['code'=>'0','state'=>'error','info'=>'传入参数不完整!']);
        }
    }
     //新增|更新客户信息
     public function set_customer(){
        $input=input('post.');
        if(isset($input['id'])){
            if(empty($input['id'])){
                //新增
                $vali = $this->validate($input,'Customer');
                if($vali===true){
                    $input['py']=zh2py($input['name']);//首拼信息
                    $create_info=Customers::create(syn_sql($input,'customer'));
                    Hook::listen('create_customer',$create_info);//客户新增行为
                   // push_log('新增客户信息[ '.$create_info['name'].' ]');//日志
                    $resule=['code'=>'1','state'=>'success'];
                }else{
                    $resule=['code'=>'0','state'=>'error','info'=>$vali];
                }
            }else{
                //更新
                $vali = $this->validate($input,'Customer.update');
                if($vali===true){
                    $input['py']=zh2py($input['name']);//首拼信息
                    $update_info=Customers::update(syn_sql($input,'customer'));
                    Hook::listen('update_customer',$update_info);//客户更新行为
                    //push_log('更新客户信息[ '.$update_info['name'].' ]');//日志
                    $resule=['code'=>'1','state'=>'success'];
                }else{
                    $resule=['code'=>'0','state'=>'error','info'=>$vali];
                }
            }
        }else{
            $resule=['code'=>'0','state'=>'error','info'=>'传入参数不完整!'];
        }
        return json($resule);
    }
    //客户列表
    public function customer_list(){
        $input=input('post.');
        //数据完整性判断
        if(isset_full($input,'page') && isset_full($input,'limit')){
            $sql=get_sql($input,[
                'name'=>'full_name_py_link',
                'number'=>'full_like',
                'contacts'=>'full_like',
                'tel'=>'full_like',
                'add'=>'full_like',
                'data'=>'full_like'
            ],'customer');//构造SQL
            $sql=auth('customer',$sql);//数据鉴权
            $count = Customers::where ($sql)->count();//获取总条数
            $arr = Customers::where($sql)->page($input['page'],$input['limit'])->order('id desc')->select();//查询分页数据
            foreach($arr as &$ve){
                $ve['merchant_name']=$ve['merchantinfo']['name'];
                if($ve['type']==2){
                    $ve['type']="修理厂";
                }else if($ve['type']==3){
                    $ve['type']="零售客户";
                }else{
                    $ve['type']="未知类型客户";
                }
            }
            $resule=[
                'code'=>1,
                'msg'=>'获取成功',
                'count'=>$count,
                'data'=>$arr
            ];//返回数据 ssh-copy-id root@1.12.70.39
        }else{
            $resule=['code'=>'0','state'=>'error','info'=>'传入参数不完整!'];
        }
        return json($resule);
    }
    //仓库列表
    public function warehouse_list(){
        $input=input('post.');
        //数据完整性判断
        if(isset_full($input,'page') && isset_full($input,'limit')){
            $sql=get_sql($input,[
                'name'=>'full_name_py_link',
                'number'=>'full_like',
                'contacts'=>'full_like',
                'tel'=>'full_like',
                'add'=>'full_like',
                'data'=>'full_like'
            ],'warehouse');//构造SQL
            if(isset($input['type'])&&$input['type']==2){
                $sql['type']=2;
            }else{
                unset($sql['type']);
            }
            $sql=auth('warehouse',$sql);//数据鉴权
            $count = Warehouses::where ($sql)->count();//获取总条数
            $arr = Warehouses::where($sql)->page($input['page'],$input['limit'])->order('id desc')->select();//查询分页数据
            $resule=[
                'code'=>1,
                'msg'=>'获取成功',
                'count'=>$count,
                'data'=>$arr
            ];//返回数据
        }else{
            $resule=['code'=>0,'state'=>'error','info'=>'传入参数不完整!'];
        }
        return json($resule);
    }
    //商品列表
    public function goods_list(){
        $input=input('post.');
        //数据完整性判断
        if(isset_full($input,'page') && isset_full($input,'limit')){
            $sql=get_sql($input,[
                'name'=>'full_name_py_link',
                'number'=>'full_like',
                'spec'=>'full_like',
                'class'=>'full_goodsclass_tree_sub',
                'brand'=>'full_eq',
                'unit'=>'full_eq',
                'code'=>'full_like',
                'location'=>'full_like',
                'data'=>'full_like'
            ],'goods');//构造SQL
            if(isset($input['type'])&&$input['type']==2){
                $sql['warehouse']=['>',1];
            }
            $count = Goodss::where ($sql)->count();//获取总条数
            $arr = Goodss::with('classinfo,unitinfo,brandinfo,warehouseinfo')->where($sql)->page($input['page'],$input['limit'])->order('id desc')->select();//查询分页数据
            $resule=[
                'code'=>0,
                'msg'=>'获取成功',
                'count'=>$count,
                'data'=>$arr
            ];//返回数据
        }else{
            $resule=['state'=>'error','info'=>'传入参数不完整!'];
        }
        return json($resule);
    }
}